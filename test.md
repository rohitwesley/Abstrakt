# Abstrakt

A holocost .......

Project Plan Document Link : https://docs.google.com/document/d/15aRlgbJysgXHsy29gFn-Cd6a7br3woYLxZ_FG-hjaeM/edit?usp=sharing
Project Files Link : https://drive.google.com/drive/folders/1WOpikU8uhA1XRi9y1x_JClyrsebT3XbS?usp=sharing
Project Map : MP_Abs_002_Templet

## Statement of Purpose

“To test out all the possible features required to create an AR-based interactive intelligent bot”

## Git : [GitHub Link] (https://github.com/rohitwesley/ARBuddy)

## Inputs :
* WASD - TBD
* Left Mouse - TBD
* Grab Left - Keyboard = Q, MotionControler = Left Grip - TBD
* Grab Right - Keyboard = Q, MotionControler = Right Grip - TBD
* Jump - Keyboard = SpaceBar, MotionControler = B - TBD
* ResetVR - Keyboard = R - TBD
* (Action)Trigger Right - Keyboard = E, MotionControler = Right Trigger - TBD
* (Acton)Trigger Left - Keyboard = E, MotionControler = Left Trigger - TBD
* Move Forward/Back - Keyboard = W/S, MotionControler = Left Thumbstick Y - TBD
* Move Right/Left - Keyboard = A/D, MotionControler = Left Thumbstick X - TBD
* Look Up/Down - Mouse Y, MotionControler = Right Thumbstick Y - TBD
* Turn Right/Left - Mouse Y, MotionControler = Right Thumbstick X - TBD

## Features :

* Integrate an animated Motion Graphics element into an AR experience.

### Stretch Goals

## Getting Started(TBD)

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites(TBD)

What things you need to install the software and how to install them

```
Give examples
```
#### Plugins
* AutoSizeComments 2.1.3
* HoudineEngine 18.0.460
* Substance in UE4 4.24.0.3
* Sun Position Calculator 1.0
* Megascans 0.10

### Installing(TBD)

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests(TBD)

Explain how to run the automated tests for this system

### Break down into end to end tests(TBD)

Explain what these tests test and why

```
Give an example
```

### And coding style tests(TBD)

Explain what these tests test and why

```
Give an example
```

## Deployment(TBD)

Add additional notes about how to deploy this on a live system

## Built With(TBD)

* [Unreal 4*24*3](https://www.unrealengine.com/en-US/) - The game engine used

## OS Platform : Windows

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [GitKraken](https://www.gitkraken.com/) for versioning. For the versions available, see the [tags on this repository](https://github.com/rohitwesley/ARBuddy/tags). 

## Authors

* **Rohit Wesley Thomas** - *Initial work* - [Technology N Art](https://www.tecrt.co/)
* **K** - *Initial work* - [Technology N Art](https://www.tecrt.co/)
* **Ellaya** - *Initial work* - [Technology N Art](https://www.tecrt.co/)

See also the list of [contributors](https://github.com/rohitwesley/ARBuddy/contributors) who participated in this project.

## License

This project is licensed under the GNU 3.0 License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
